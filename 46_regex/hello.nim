import regex
let r: Regex = re"\w\d"
# the \w stands for a word character that includes upper and lower case ASCII letters, and the \d stands for a decimal digit
let t1: string = "a1"
let t2 = "nim"
var m: RegexMatch
if match(t1, r, m):
  echo "match t1"
if match(t2, r, m):
  echo "match t2"
