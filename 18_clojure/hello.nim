type f=proc(a:int):int 
func makeAdder(x:int):f=
    return proc(y:int):int =
        return x+y

let add5=makeAdder(5)
echo add5(7)
