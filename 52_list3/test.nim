import std/lists
# Create a mutable list of integers
var myList = @[1, 2, 3]
# Add an element to the end of the list
myList.add(4)
# Insert an element at the beginning of the list
myList.insert(0, 0)
# Remove the last element from the list
var x= myList.pop
# Print the list
echo myList
# Get the length of the list
echo myList.len
