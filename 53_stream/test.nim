import std/streams

var strm = newStringStream("""The first line
the second line
the third line""")

var line = ""

while strm.readLine(line):
  echo line

# Output:
# The first line
# the second line
# the third line

strm.close()
