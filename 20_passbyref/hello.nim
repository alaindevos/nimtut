proc funa (x:int):int =
    var y=x+1
    return y

proc funb (x:var int):int = # pass by reference
    x=x+1
    return x

var z=1
echo z       
echo funb(z)
echo z
echo funa(z)

