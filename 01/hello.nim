import std/strformat
import std/strutils
import std/sequtils
import std/tables
import std/sugar

# Comment
echo "Hello World"
"Hello World".echo
write(stdout,"Hello World")
var age : int =14
age=13
echo age
var name : string ="Alain"
echo "Name:",name
#string
var a2="Alain"
var b2=" De Vos"
var c2= a2 & b2
echo c2
echo c2[0]
echo c2[0..1]
echo fmt"Name is {c2}"
var z : string = "15"
echo z.type()
var zz : int = z.parseInt()
echo zz
if 1<2:
    echo "smaller"
    echo "smaller"    
else:
    echo "larger"
block:
    const zzz=5
    echo zzz
const c= 5
case c:
    of 1,3,5,7:
        echo  "c is odd"
    of 2,4,6,8:
        echo " c is even"
    else:
        discard
type
    Color = enum RED , GREEN
const textColor = Color.RED
case textColor:
    of Color.RED:
        echo "RED"
    of COlor.GREEN:
        echo "GREEN"
var names=["Alain","Eddy"]
echo names[0]
var intvar3 : array[3,int]=[1,2,3]
var ii = intvar3[1..2]
echo ii
var intvar2 : array[2,int]
intvar2[0]=1
intvar2[1]=2
var list: seq[string]
list= @["Alain","Eddy"]
list.add("Jan")
list.del(1)
echo list
echo list[1]
var intlist= @[1,2,3,4,5,6,7]
let il=intlist.filter( proc (x:int):bool = x < 4 )
let il2=intlist.map( proc (x:int):int = 2 * x )
echo il
echo il2
#tuple
var t1:(string,int)=("Alain",21)
echo t1[0]," ",t1[1]
#named tuple
var t2:tuple [ name : string , age : int ] =("Eddy",44)
t2.name="Jan"
echo t2.name
var tel = 0
while tel < 10 :
    tel=tel+1
    echo tel
    if tel > 4:
        break
var friends = @["Alain","Eddy","Jan"]
for index,friend in friends:
    echo index," | ",friend
for i in 1..4:
    echo i
var hash= initTable[string,int]()
hash["Alain"]=21
hash["Eddy"]=42
echo hash["Alain"]
echo hash.hasKey("Alain")
echo hash.getOrDefault("Alain")
proc sayhello(name:string="Alain"):void =
    echo "Hello ",name
sayhello()
sayhello("Eddy")
proc sumx(x:int,y:int):int = return (x + y)
echo  sumx(3,5)
# Pass by reference
proc incx(x:var int):void =
    x=x+1
var p=2
p.incx
echo p
#overloading
proc `+`(x:string,y:string):string=x&y
var s="Alain"+" De Vos"
echo  s
proc mulx(x:int,y:int):int=return x*y
proc mul2(myfun:(int,int) -> int,x:int,y:int):int=return myfun(x,y)
echo mulx(3,5)
echo mul2(mulx,3,5)
#Datatypes
type
    Person=tuple[name:string,age:int]
var jack:Person=("Jack",23)
echo jack.name
type
    Dog = ref object
        name:string
        age:int
var ddd=Dog(name:"Jack",age:10)
echo ddd.name
proc speak(self: Dog, msg: string) = 
  echo self.name & " says:" & msg
ddd.speak("Woef") 