
import std/lists
import std/sequtils

var list = initSingLyLinkedList[int]()
let
  a = newSingLyLinkedNode[int](3)
  b = newSingLyLinkedNode[int](7)
  c = newSingLyLinkedNode[int](9)

list.add(a)
list.add(b)
list.prepend(c)

assert a.next == b
assert b.next == nil
echo list.toSeq[1]

type
    ThreeStrings=array[3,string]
let names:ThreeStrings=["Alain","Eddy","Jos"]
echo names[2]

