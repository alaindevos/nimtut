import std/options

proc getLetterPos(s:string,c:char):Option[int] =
    for index,character in s:
        if character==c:
            return some(index)
    return none(int)

let a=getLetterPos("abcdef",'c')
let b=getLetterPos("abcdef",'x')
if isSome(a):
    var aa=get(a)
    echo aa
if isNone(b):
    echo "Is not part"

let c=some(some(some(some(6))))
echo get(get(c))  # some(some(6))


