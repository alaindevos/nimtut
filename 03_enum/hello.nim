type
    Kind = enum Int , String 
    C = ref object
        case kind: Kind:
        of Int:
            i: int
        of String:
            s: string
var C1=C(kind:Int,i:123)
var C2=C(kind:String,s : "Alain" )

proc printC(c : C) : void =
    case c.kind
        of Int:
            echo c.i
        of String:
            echo c.s
printC(C1)
printC(C2)

