import strutils
import std/re
import db_connector/db_postgres
let db = open("127.0.0.1", "x", "x", "syslogng")
var ar= db.get_all_rows(sql"select * from messages_freebsd_20240130")
var myregex1=re"console"
var myregex2=re"syslog"
for r in ar:
    var res=" | " & align(r[0],15) &
            " | " & align(r[1],7) &
            " | " & alignleft(r[2],9) &
            " | " & align(r[3],6,'0') &
            " | " & alignleft(r[4],6) &
            " | " & alignleft(r[5],7) &
            " | " & alignleft(r[6],80)[0..79] &
            " | "
    if (contains(res,myregex1)) and (not (contains(res,myregex2))):
        echo res
db.close()
