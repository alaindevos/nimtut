import malebolgia
import os

proc f(i:int)=
  sleep 4
  echo i
  
var m=createMaster()
for i in 0..5:
  m.spawn f(i)
sleep 2
echo "Too soon"
m.awaitAll:
  sleep 1
echo "Done"
