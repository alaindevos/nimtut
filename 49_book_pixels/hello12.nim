type
  NodeKind = enum
    nkInt,
    nkFloat,
    nkLR
  Node = ref NodeObj
  NodeObj = object
    case kind: NodeKind 
    of nkInt   : intVal  : int
    of nkFloat : floatVal : float
    of nkLR    : lrVal   : (Node,Node)
var n1= Node(kind:nkInt,intVal:5)
var n2= Node(kind:nkLR,lrVal:(n1,n1))
var (x,y)=n2.lrVal
echo x.intVal
