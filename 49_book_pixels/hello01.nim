import pixels

type
    Point = object
        x : int
        y : int
let a=Point(x:60,y:48)

proc drawHorizontalLine(start:Point,length:Positive)=
    for delta in 0..length:
        putPixel(start.x+delta,start.y)

proc drawVerticalLine(start:Point,length:Positive)=
    for delta in 0..length:
        putPixel(start.x,start.y+delta)

drawHorizontalLine(a,50)
drawVerticalLine(a,30)
        