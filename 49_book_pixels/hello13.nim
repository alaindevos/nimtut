type
  PrintString= proc(s:string){.closure.}

proc printstring(s:string) =
  echo s

proc printme(s:string,f:PrintString) =
  f(s)

printme("Hallo",printstring)
