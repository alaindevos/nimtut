import pixels

type
    Point = object
        x : int
        y : int

proc resetPointsToOrigin(points: var seq[Point])=
    for i in 0 .. points.len-1: 
        points[i]=Point(x:0,y:0)

var points = @[Point(x:2,y:4), Point(x:3,y:3)]
resetPointsToOrigin points 

        