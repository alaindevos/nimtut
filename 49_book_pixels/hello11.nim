type
  Persona=tuple[name:string,age:int]
var 
  person1:Persona
  person2:Persona
person1=(name:"Peter",age:30)
person2=("Eddy",50)

type
  Personb = tuple
    name:string
    age:int
var 
  person3:Personb
person3=("Alain",40)

type
  Personc = object of RootObj
    name:string
    age:int
  Personcref=ref Personc
  Student= ref object of Personc 
    id:int
var
  personc:Personc
  student:Student
personc=Personc(name:"Alain",age:30)
var personcref=Personcref(name:"Jan",age:35)
personcref.name="Louis"
personc.name="Frans"
student=Student(name:"Eddy",age:40,id:3)
