import strutils, tables

var
  arr = split("Blue Blue Red Green", " ")     # list of words
  uniqarr = initTable[string, int]()          # array to hold unique words

for i in arr:                                 # unique the list
  uniqarr[i] = 1

for j in pairs(uniqarr):                      # echo the list
  echo j
  
