func sum(x,y:int):int=x+y

type MathFuncType=proc(a:int,b:int):int

proc processFunc(mathFunc:MathFuncType,a:int,b:int):int =
    mathFunc(a,b)

echo sum(1,2)

echo processFunc(sum,1,2)


