import std/sequtils
import std/sugar
var s:seq[int]
s.add(1)
s.add(2)
s.add(3)
s[1]=4
var s2=s[1..2]
var s3=concat(@[1,2],@[3,4])
echo s.min
echo s.max
echo s.deduplicate
s3.insert(3,3)
s3.delete(2,3)
echo s3.map(proc(x:int):int=x*x)
echo s3.map(x=>x*x)
echo s3.mapIt(it*it)
var s4=s3.filter(proc(x:int):bool=(x and 1)==0)
echo s4
var s5=(0..9).toSeq
echo s5
s5.apply(x=>x*x)


