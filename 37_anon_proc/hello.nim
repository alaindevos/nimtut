import std/sequtils
import std/sugar
import std/math
var isEven2= proc(x:int):bool=x mod 2 == 0
var square=proc(x:int):int=x*x
proc isEven1(x:int):bool =
  var (d,m)=divmod(x,2)
  return (m==0)
let s=toSeq(1..10)
var r1=s.filter(isEven1)
echo r1
var r2=s.map(square)
echo r2
var r3=s.map(x=>x*x)
echo r3


