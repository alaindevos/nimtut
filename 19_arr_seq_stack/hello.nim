proc myarr()=
    const arr=[3,4,5]
    for a in arr:
        echo a
myarr()

proc myseq()=
    var x:seq[int]
    x.add(3)
    x.add(4)
    x.add(5)
    for a in x:
        echo a
myseq()

type Stack=seq[int]

proc push(stack:var Stack,item:int) = # var pass by reference
    stack.add(item)

proc pop(stack:var Stack):int =
    if (stack.len > 0):
        let lastIndex=stack.len-1
        let item=stack[lastIndex]
        stack.del(lastIndex)
        return item
    else:
        raise newException(ValueError,"Stack is empty")

proc isEmpty(stack:Stack):bool =
    return stack.len == 0

proc clear(stack:var Stack) =
    stack.setLen(0)

var myStack:Stack
for i in 1..3:
    push(myStack,i)

echo "Stack:",myStack

while not isEmpty(myStack):
    let item=pop(myStack)
    echo "Popped item:",item,"   Stack:",myStack




