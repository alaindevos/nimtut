type
    B = ref object of RootObj
            i : int
            s : string
    S = ref object of B 
            b : bool
var b=B(i:5,s:"Alain")
var s=S(i:4,s:"Eddy",b:true)
