import std/sequtils
import std/sugar

var funa= proc(x:int):int=x*2
var funb= proc(x:int):bool=x mod 6 != 0
var func1= proc(x:int):bool= x>5
let s=toSeq(1..10)
let foo1 = s.map(x => x * 2).filter(x => x mod 6 != 0)
echo foo1
let foo2 = s.map(funa).filter(funb)
echo foo2
var f1=s.filter do (x:int) -> bool: x>5
var f2=s.filter(func1)
echo f1
echo f2
