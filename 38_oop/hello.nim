type
  Circle = object
    radius:int
  Rectangle = object
    x:int
    y:int
template mysize(c:Circle):int=3*c.radius*c.radius
template mysize(r:Rectangle):int=r.x*r.y

var c:Circle=Circle(radius:10)
var r:Rectangle=Rectangle(x:3,y:4)
echo mysize(c)
echo mysize(r)
