type Animal = object
  name: string
  age: int
  
proc speak(self: Animal, msg: string) = 
  echo self.name & " says:" & msg 
  
var sparky = Animal(name: "Sparky", age: 10)

sparky.speak("Hi")
